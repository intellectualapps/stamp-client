/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stamp.client;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author auwalms
 */
public class ManageUsers extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie[] cookies = request.getCookies();

      boolean showDashboard = false;

      if (cookies != null) {
          for (int i = 0; i < cookies.length; i++) {
              String name = cookies[i].getName();
              String value = cookies[i].getValue();
              if (name.equals("stamp-authToken")) {
                  showDashboard = true;
                  break;
              }
          }
      }

      if (showDashboard) {
          request.getRequestDispatcher("WEB-INF/pages/manage-users-frame.html").forward(request, response);
      } else {
          response.sendRedirect("/login");
      }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

  }

  @Override
  public String getServletInfo() {
    return "Servlet to manage the Manage users page 'manage-users.html'";
  }

}
