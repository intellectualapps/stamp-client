/**
 * @author auwalms
 * @desc Add Worker Relationship Javascript file
 */

$(function () {
    getUserDetail(identifier);
    var userDetails = JSON.parse(localStorage.getItem('userDetails'));

        $('#WorkerRelForm').on("submit", function (event) {
            event.preventDefault();
            var dataString = $("#WorkerRelForm").serialize();
            var fullName = $('#employee-first-name').val() + ' ' + $('#employee-last-name').val();
            $('#createRelationship').addClass('disabled');
            addWorkerRelationship(dataString, fullName);

    });

    $('#use-company').click(function () {
        if ($('#use-company').is(':checked')) {
            document.querySelector('#selectCompany').removeAttribute('disabled');
        }
    });

    //use datepicker
    datePicker();
});


/**
 * Update Employers Details on fetch
 */

function updateEmployerFields(response) {
    $('#primary-c2t-id').val(response.c2tId || 'NA');
    $('#employer-first-name').val(response.firstName || 'NA');
    $('#employer-last-name').val(response.lastName || 'NA');
    $('#employer-phone-number').val(response.phoneNumber || 'NA');
    $('#employer-email').val(response.email || 'NA');
}

/**
 * Update Employees Details on after getting details from localstorage.
 */
function updateEmployeeFields(response) {
    $('#employee-first-name').val(response.firstName || 'NA');
    $('#employee-last-name').val(response.lastName || 'NA');
    $('#employee-phone-number').val(response.phoneNumber || 'NA');
    $('#employee-email').val(response.email || 'NA');

    var employeeDiv = document.querySelector('#employeeDiv');
    var regLink = document.querySelector('#regPerson');
    if (regLink)
        employeeDiv.removeChild(regLink);
}


/**
 * Add Worker Relationship
 */
function addWorkerRelationship(details, fullName) {
    hidePage();
    $.ajax({
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + authToken);
            },
            method: "POST",
            url: apiBaseUrl + "relationship/worker",
            data: details,
            dataType: "json"

        })
        .done(function (response) {
            showPage();
            $('#createRelationship').removeClass('disabled');
            if (response.status) {
                $('#WorkerRelForm').prepend('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error! </strong>' + response.message + '</div>');
                setTimeout(function(){
                    $(".alert").remove();  
                },2000);
            } else {
                $('html, body').animate({scrollTop: 0}, 'fast');
                window.localStorage.setItem('relatedPersonFullName', fullName);
                $('#WorkerRelForm').prepend('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success! </strong>Relationship Added Successfully</div>');
                var verify = window.prompt('Do you wish to verify ' + fullName)
                // verify? window.location.href = '/verify-user' : window.location.href = '/relationships-dashboard'
                setTimeout(function () {
                    (document.referrer) ? window.location.href = document.referrer : verify ? window.location.href = '/verify-relationship' : window.location.href = '/relationships-dashboard';
                    $(".alert").remove();
                }, 2000)
            }
        })
        .fail(function (error) {
            showPage();
            $('#createRelationship').removeClass('disabled');
            $('#WorkerRelForm').prepend('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error! </strong>' + error.message + '</div>');
            setTimeout(function(){
                $(".alert").remove();  
            },2000);
        });
}

/**
 * fetch the company created by the logged in user using the user's id
 * */

function getUserCompany(userId) {
    $.ajax({
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + authToken);
            },
            url: apiBaseUrl + 'user/companies',
            method: 'GET',
            data: 'c2t-id=' + userId,
            dataType: 'json'
        })
        .done(function (response) {
            if (response.status) {
                //returned nothing
            } else {
                var selectCompany = $('#selectCompany');
                $.each(response, function (key, value) {
                    selectCompany.append('<option value=' + value.companyC2tId + '>' + value.companyName + '</option>');
                });

            }
        })
        .fail(function (response) {
            $('#WorkerRelForm').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning! </strong>' + response.message + '</div>');
            setTimeout(function () {
                $(".alert").remove();
            }, 3000);
        });
}
/**
 * fetch company detail on select of a company from companies options
 * */
function fetchCompanyDetails(companyId) {
    if (companyId === '-- Select company --') return;

    var employerDiv = document.querySelector('#employerDiv');
    var personDetail = document.querySelector('#person-div');


    $.ajax({
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + authToken);
            },
            url: apiBaseUrl + 'company/' + companyId,
            method: 'GET',
            dataType: 'json'
        })
        .done(function (response) {
            if (response.status) {
                //returned nothing
            } else {
                if (personDetail)
                    employerDiv.removeChild(personDetail);

                showCompanyDetails(response);
            }
        })
        .fail(function (response) {
            $('#WorkerRelForm').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning! </strong>' + response.message + '</div>');
            setTimeout(function () {
                $(".alert").remove();
            }, 3000);
        });
}

function showCompanyDetails(companyData) {
    var companyDetails = document.querySelector('#company-details');
    companyDetails.innerHTML = '';

    var idFormGroupDiv = document.createElement('div');
    idFormGroupDiv.setAttribute('class', 'form-group');
    var idLabel = document.createElement('label');
    idLabel.setAttribute('for', 'primary-c2t-id');
    idLabel.setAttribute('class', 'control-label col-md-4');
    idLabel.innerHTML = 'Company C2TID';
    var idInputDiv = document.createElement('div');
    idInputDiv.setAttribute('class', 'col-md-6');
    var idInputField = document.createElement('input');
    idInputField.setAttribute('type', 'text');
    idInputField.setAttribute('id', 'primary-c2t-id');
    idInputField.setAttribute('name', 'primary-c2t-id');
    idInputField.setAttribute('class', 'form-control');
    idInputField.setAttribute('readonly', '');
    idInputField.value = companyData.companyC2tId || 'NA';
    idInputDiv.appendChild(idInputField);
    idFormGroupDiv.appendChild(idLabel);
    idFormGroupDiv.appendChild(idInputDiv);

    var nameFormGroupDiv = document.createElement('div');
    nameFormGroupDiv.setAttribute('class', 'form-group');
    var nameLabel = document.createElement('label');
    nameLabel.setAttribute('for', 'company-name');
    nameLabel.setAttribute('class', 'control-label col-md-4');
    nameLabel.innerHTML = 'Company Name';
    var nameInputDiv = document.createElement('div');
    nameInputDiv.setAttribute('class', 'col-md-6');
    var nameInputField = document.createElement('input');
    nameInputField.setAttribute('type', 'text');
    nameInputField.setAttribute('id', 'company-name');
    nameInputField.setAttribute('class', 'form-control');
    nameInputField.setAttribute('readonly', '');
    nameInputField.value = companyData.companyName || 'NA';
    nameInputDiv.appendChild(nameInputField);
    nameFormGroupDiv.appendChild(nameLabel);
    nameFormGroupDiv.appendChild(nameInputDiv);

    var addressFormGroupDiv = document.createElement('div');
    addressFormGroupDiv.setAttribute('class', 'form-group');
    var addressLabel = document.createElement('label');
    addressLabel.setAttribute('for', 'company-address');
    addressLabel.setAttribute('class', 'control-label col-md-4');
    addressLabel.innerHTML = 'Address';
    var addressTextareaDiv = document.createElement('div');
    addressTextareaDiv.setAttribute('class', 'col-md-6');
    var addressTextareaField = document.createElement('textarea');
    addressTextareaField.setAttribute('cols', '30');
    addressTextareaField.setAttribute('rows', '5');
    addressTextareaField.setAttribute('id', 'company-address');
    addressTextareaField.setAttribute('class', 'form-control');
    addressTextareaField.setAttribute('readonly', '');
    addressTextareaField.value = companyData.addressLine || 'NA';
    addressTextareaDiv.appendChild(addressTextareaField);
    addressFormGroupDiv.appendChild(addressLabel);
    addressFormGroupDiv.appendChild(addressTextareaDiv);

    companyDetails.appendChild(idFormGroupDiv);
    companyDetails.appendChild(nameFormGroupDiv);
    companyDetails.appendChild(addressFormGroupDiv);
}