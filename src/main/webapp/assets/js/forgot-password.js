$(function () {
  $('#forgotPasswordForm').on("submit", function (event) {
    event.preventDefault();
    var dataString = $("#forgotPasswordForm").serialize();
    submitEmail(dataString);
  });
});

/**
 * Submit User Email for reset
 */
function submitEmail(details) {
  hidePage();
  $.ajax({
    method: "POST",
    url: apiBaseUrl + "account/password",
    data: details,
    dataType: "json"
  })
    .done(function (response) {
      showPage();
      $("#resetBtn").removeClass("disabled");
      if (response.status) {
          $("#forgotPasswordForm").prepend('<div class="alert alert-danger" id="resetStatus">' + response.message + '</div>');
          setTimeout(function() {
            $('.alert').remove();
          },2000)
      } else {
        $("#forgotPasswordForm").prepend('<div class="alert alert-success" id="resetStatus">Reset link sent, Please check your Email</div>');
        setTimeout(function() {
          $('.alert').remove();
        },2000)
      }
    })
    .fail(function (error) {
      showPage();
      $("#resetBtn").removeClass("disabled");
      $("#forgotPasswordForm").prepend('<div class="alert alert-danger" id="resetStatus">' + error.message + '</div>');
      setTimeout(function() {
        $('.alert').remove();
      },2000)
    });
}