$(function () {
  $('#loginForm').on("submit", function (event) {
    event.preventDefault();
    $("#loginStatus").remove();
    $("#loginBtn").addClass("disabled");
    var dataString = $("#loginForm").serialize();
    loginUser(dataString);
  });

});


function loginUser(details) {
  hidePage();
  $.ajax({
      method: "POST",
      url: apiBaseUrl + "user/account/authenticate?" + details,
      dataType: "json"
    })
    .done(function (response) {
      showPage();
      $("#loginBtn").removeClass("disabled");
      if (response.authToken) {
        setCookie("stamp-identifier", response.username);
        setCookie("stamp-authToken", response.authToken);
        setCookie("stamp-email", response.email);
        setCookie("stamp-phoneNumber", response.phoneNumber);
        setCookie("stamp-fullname", response.firstName + " " + response.lastName);
        setCookie("stamp-account", "person");
        $("#loginForm").prepend('<div class="alert alert-success" id="loginStatus">Login successful</div>');
        setTimeout(function () {
          (document.referrer) ? window.location.href = document.referrer: window.location.href = '/dashboard';
        }, 1000);
      } else {
        showPage();
        $("#loginForm").prepend('<div class="alert alert-danger" id="loginStatus">' + response.message + '</div>');
        setTimeout(function(){
          $('.alert').remove();
        },2000);
      }
    })
    .fail(function (error) {
      showPage();
      $("#loginBtn").removeClass("disabled");
      $("#loginForm").prepend('<div class="alert alert-danger" id="loginStatus">' + error.message + '</div>');
      setTimeout(function(){
        $('.alert').remove();
      },2000);
    });
}