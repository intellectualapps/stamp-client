/**
 * @author auwalms
 * @desc General Javascript file
 */
(function() {
  $("#manageUsersForm").on("submit", function(event) {
    event.preventDefault();
    $("#searchBtn").addClass("disabled");
    // var dataString = $("#manageUsersForm").serialize();
    // console.log(dataString);
    searchUserByParam();
    // searchUserByParam(dataString);
  });
})();

/**
 * function called after the user
 * clicks the search button
 */
function searchUserByParam() {
  hidePage();
  $.ajax({
    beforeSend: function(xhr) {
      xhr.setRequestHeader("Authorization", "Bearer " + authToken);
    },
    method: "GET",
    url: apiBaseUrl + "account/all",
    dataType: "json"
  })
    .done(function(response) {
      showPage();
      $("#searchBtn").removeClass("disabled");
      if (response.status) {
        $("#manageUsersForm").prepend(
          '<div class="alert alert-danger alert-dismissible" role="alert" id="searchStatus"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error! </strong>' +
            response.message +
            "</div>"
        );
        setTimeout(function() {
          $(".alert").remove();
        }, 2000);
      } else {
        populateSearchTable(response.accounts);
      }
    })
    .fail(function(error) {
      showPage();
      $("#searchBtn").removeClass("disabled");
      $("#manageUsersForm").prepend(
        '<div class="alert alert-danger" id="searchStatus">' +
          error.message +
          "</div>"
      );
      setTimeout(function() {
        $(".alert").remove();
      }, 2000);
    });
}

/**
 * populates the search table with result of the API call
 */
function populateSearchTable(searchResult) {
  $("#searchResultTableBody").html("");

  var tbody = document.getElementById("searchResultTableBody");
  var searchResultLength = searchResult.length;
  if (searchResultLength > 0) {
    for (var i = 0; i < searchResultLength; i++) {
      var index = i + 1;
      var fullName =
        searchResult[i].firstName + " " + searchResult[i].lastName || "NA";
      var emailAddress = searchResult[i].email || "NA";
      var userName = searchResult[i].username || "NA";
      var phoneNumber = searchResult[i].phoneNumber || "NA";
      var role = searchResult[i].role || "User";
      var archiveBtn = document.createElement("button");
      archiveBtn.innerHTML = "Archive";
      archiveBtn.setAttribute("class", "btn btn-danger btn-xs");
      archiveBtn.setAttribute("id", "archiveBtn");
      archiveBtn.setAttribute("type", "button");
      archiveBtn.setAttribute("onclick", "archiveUser(" + index + ")");

      var editBtn = document.createElement("button");
      editBtn.innerHTML = "Edit";
      editBtn.setAttribute("class", "btn btn-info btn-xs");
      editBtn.setAttribute("id", "editBtn");
      editBtn.setAttribute("type", "button");
      editBtn.setAttribute("onclick", "editUser(" + index + ")");

      var tr = document.createElement("tr");

      var indexTd = document.createElement("td");
      indexTd.innerHTML = index;

      var fullNameTd = document.createElement("td");
      fullNameTd.innerHTML = fullName;

      var emailAddressTd = document.createElement("td");
      emailAddressTd.innerHTML = emailAddress;

      var userNameTd = document.createElement("td");
      userNameTd.innerHTML = userName;

      var phoneNumberTd = document.createElement("td");
      phoneNumberTd.innerHTML = phoneNumber;

      var roleTd = document.createElement("td");
      roleTd.innerHTML = role;

      var actionRowTd = document.createElement("td");
      actionRowTd.append(archiveBtn);
      actionRowTd.append(editBtn);

      tr.appendChild(indexTd);
      tr.appendChild(fullNameTd);
      tr.appendChild(emailAddressTd);
      tr.appendChild(userNameTd);
      tr.appendChild(phoneNumberTd);
      tr.appendChild(roleTd);
      tr.appendChild(actionRowTd);

      tbody.appendChild(tr);
    }
    var tb_st = true;
  } else {
    var tr = document.createElement("tr");
    var td = document.createElement("td");
    td.innerHTML =
      "<h1>No persons matching your search criteria was found</h1>";
    td.className = "center";
    td.setAttribute("colspan", "4");
    tr.appendChild(td);
    tbody.appendChild(tr);
  }

  if (tb_st) {
    $("#searchResultTable").DataTable({
      destroy: true,
      pageLength: 50,
      searching: false,
      ordering: false
    });
  }
}

function archiveUser(index) {
  var $user = index;
  alert("archiving user number ", $user);
}

function editUser(index) {
  var $user = index;
  alert("editing user number ", $user);
}
