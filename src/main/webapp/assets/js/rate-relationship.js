/**
 * @author auwalms
 * @desc Rate Relationship Javascript file
 */

$(function () {
    var fullName = window.localStorage.getItem('relatedPersonFullName');
    document.querySelector('#userFullName').innerHTML = fullName;

    if (currentUrl.indexOf('rate-relationship') != -1) {
        if (!fullName) {
            document.querySelector('#submitRatingBtn').classList.add('disabled');
            return;
        }

        document.querySelector('#notNow').addEventListener('click', function () {
            window.location.href = '/relationships-dashboard'
        });

        document.querySelector('#rateUserForm').addEventListener('submit', function (e) {
            e.preventDefault();
            var dataString = $('#rateUserForm').serialize();
            document.querySelector('#submitRatingBtn').classList.add('disabled');
            rateUser(dataString);

        })
    }

    document.querySelector('#verifyBtn').addEventListener('click', verifyRelationship);


});

function rateUser(dataString) {
    hidePage();
    $.ajax({
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + authToken);
        },
        method: "POST",
        url: apiBaseUrl + "",
        data: dataString,
        dataType: "json"
    })
        .done(function (response) {
            showPage();
            $('#submitRatingBtn').removeClass('disabled');
            if (response.status) {
                $('#rateUserForm').prepend('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error! </strong>' + response.message + '</div>');
                setTimeout(function(){
                    $('.alert').remove;
                },2000);
            } else {
                $('html, body').animate({scrollTop: 0}, 'fast');
                $('#rateUserForm').prepend('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success! </strong>Rating was submitted successfully</div>');
                setTimeout(function () {
                    window.location.href = '/relationships-dashboard';
                }, 2000)
            }
        })
        .fail(function (error) {
            document.querySelector('#submitRatingBtn').classList.remove('disabled');
            $('#rateUserForm').prepend('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error! </strong>' + error.message + '</div>');
            setTimeout(function(){
                $('.alert').remove;
            },2000)
        });
}

function verifyRelationship() {
    $('#verifyAlertBox').append('<strong>Success! </strong>Relationship Verified');
    $('#verifyAlertBox').addClass('bg-success');
    setTimeout(function () {
        window.location.href = '/rate-relationship'     
    },3000);  

}