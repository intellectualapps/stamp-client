/**
 * @author auwalms
 * @desc Register Company Javascript file
 */

var telNumberCount = 1;
var contactNumberCount = 1;
var dynamicContactDiv = document.querySelector('#dynamicContact');
var dynamicTelephoneDiv = document.querySelector('#dynamicTel');

$(function () {
    fetchStates();

    populateCompanyForm();
    
    $('#addTelephone').on('click', function () {
        if (telNumberCount >= 3) {
            $('#addTelephone').addClass('disabled');
            return;
        }
        telNumberCount++;
        addTelephone(telNumberCount);
    });

    
    $('#addContact').on('click', function () {
        if (contactNumberCount >= 3) {
            $('#addContact').addClass('disabled');
            return;
        }
        contactNumberCount++;
        addContact(contactNumberCount);
    });

    $('#newCompanyForm').on("submit", function (event) {
        event.preventDefault();
        var dataString = $("#newCompanyForm").serialize();
        $('#saveBTN').addClass('disabled');
        registerCompany(dataString);
    });

});

function addTelephone(telNumberCount) {    
    var dynamicDivWrap = document.createElement('div');    
    dynamicDivWrap.setAttribute('class', 'dynamicWrapper');

    var FormGroupDiv = document.createElement('div');
    FormGroupDiv.setAttribute('class', 'form-group');
    
    var label = document.createElement('label');
    label.setAttribute('for', 'company-telephone' + telNumberCount);
    label.setAttribute('class', 'control-label col-md-4');
    label.innerHTML = 'Phone ' + telNumberCount;
    var InputDiv = document.createElement('div');
    
    InputDiv.setAttribute('class', 'col-md-8');
    var inputField = document.createElement('input');
    inputField.setAttribute('type', 'tel');
    inputField.setAttribute('name', 'company-telephone-' + telNumberCount);
    inputField.setAttribute('id', 'company-telephone-' + telNumberCount);
    inputField.setAttribute('class', 'form-control');
    InputDiv.appendChild(inputField);
    
    FormGroupDiv.appendChild(label);
    FormGroupDiv.appendChild(InputDiv);
    
    var removeBtn = document.createElement('span');
    removeBtn.setAttribute('class', 'glyphicon glyphicon-remove-circle red-remove');
    removeBtn.setAttribute('id', 'removeTelephone' + telNumberCount);
    removeBtn.setAttribute('type', 'button');
    removeBtn.setAttribute('onclick', 'removeDynamicTelephoneField(event)');

    dynamicDivWrap.appendChild(removeBtn);
    dynamicDivWrap.appendChild(FormGroupDiv);
    
    var dynamicDiv = document.querySelector('#dynamicTel');
    dynamicDiv.appendChild(dynamicDivWrap);
}

function addContact(contactNumberCount) {
    var dynamicDivWrap = document.createElement('div');    
    dynamicDivWrap.setAttribute('class', 'dynamicWrapper');
    
    var FormGroupDiv = document.createElement('div');
    FormGroupDiv.setAttribute('class', 'form-group');
    
    var label = document.createElement('label');
    label.setAttribute('for', 'contact-person' + contactNumberCount);
    label.setAttribute('class', 'control-label col-md-4');
    label.innerHTML = 'Contact ' + contactNumberCount;
    
    var InputDiv = document.createElement('div');
    InputDiv.setAttribute('class', 'col-md-8');    
    var inputField = document.createElement('input');
    inputField.setAttribute('type', 'text');
    inputField.setAttribute('name', 'contact-person-' + contactNumberCount);
    inputField.setAttribute('id', 'contact-person-' + contactNumberCount);
    inputField.setAttribute('class', 'form-control');
    InputDiv.appendChild(inputField);
    
    FormGroupDiv.appendChild(label);
    FormGroupDiv.appendChild(InputDiv);
    
    var removeBtn = document.createElement('span');
    removeBtn.setAttribute('class', 'glyphicon glyphicon-remove-circle red-remove');
    removeBtn.setAttribute('id', 'removeContact' + contactNumberCount);
    removeBtn.setAttribute('type', 'button');
    removeBtn.setAttribute('onclick', 'removeDynamicContactField(event)');

    dynamicDivWrap.appendChild(removeBtn); 
    dynamicDivWrap.appendChild(FormGroupDiv);

    var dynamicContact = document.querySelector('#dynamicContact');
    dynamicContact.appendChild(dynamicDivWrap);
}

function registerCompany(details) {
    hidePage();
    $.ajax({
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + authToken);
            },
            method: "POST",
            url: apiBaseUrl + "company",
            data: details,
            dataType: "json"

        })
        .done(function (response) {
            showPage();
            $('#saveBTN').removeClass('disabled');
            if (response.status) {
                $('#newCompanyForm').prepend('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error! </strong>' + response.message + '</div>');
                setTimeout(function () {
                  $('.alert').remove();  
                }, 2000);
            } else {
                $('html, body').animate({
                    scrollTop: 0
                }, 'fast');
                $('#newCompanyForm').prepend('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success! </strong>New Company Added Successfully</div>');
                setTimeout(function () {
                    (document.referrer) ? window.location.href = document.referrer: window.location.href = '/dashboard';
                }, 2000);
            }
        })
        .fail(function (error) {
            showPage();
            $('#saveBTN').removeClass('disabled');
            $('#newCompanyForm').prepend('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error! </strong>' + error.message + '</div>');
            setTimeout(function(){
                $('.alert').remove();
            },2000)
        });
}

function populateCompanyForm() {
    //register company fields
    $('#registrar-c2t-id').val(getCookie("stamp-identifier") || 'NA');
    $('#contact-c2t-ids').val(getCookie("stamp-identifier") || 'NA');
    $('#email').val(getCookie("stamp-email") || 'NA');
    $('#phone-numbers').val(getCookie("stamp-phoneNumber") || 'NA');
}

function removeDynamicContactField (event) {
    var removeBtnDiv = $(event.target).attr('id');
    var buttonParent = document.querySelector("#" + removeBtnDiv).parentNode;
    dynamicContactDiv.removeChild(buttonParent);
    contactNumberCount--;
}

function removeDynamicTelephoneField (event) {
    var removeBtnDiv = $(event.target).attr('id');
    var buttonParent = document.querySelector("#" + removeBtnDiv).parentNode;
    dynamicTelephoneDiv.removeChild(buttonParent);
    telNumberCount--;
}