/**
 * @author ahassan
 * @desc Register Person Javascript file
 */

var counter = 1;
$(function () {

  $('[data-toggle="tooltip"]').tooltip();
  $("#username").keydown(function () {
    $(this).closest('.form-group').removeClass('has-error');
  });

  $("#username").blur(function () {
    var username = $(this).val();
    if (username !== '') {
      verifyUsername(username);
    } else {
      $(this).closest('.form-group').addClass('has-error');
    }
  });

  if (currentUrl.includes('register-self')) {
    getAccountDetail(identifier);
  } else if (currentUrl.includes('register-person')) {
    getUserDetailsById(identifier);
  }

  //fetch state options
  fetchStates();

  //fetch gender options
  fetchGender();

  //fetch marital status options
  fetchMaritalStatus();

  // fetch identification-type options
  fetchIdentificationTypes('id-type1');

  // fetch title-type options
  fetchTitles();

  /*
   * check2Trust register-person form submit
   */
  $('#newPersonForm').on("submit", function (event) {
    event.preventDefault();
    var bvn_input = $('#bvn').val();
    if (bvn_input.length != 11) {
      $('#newPersonForm').prepend('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Invalid BVN!</strong>Please enter your 11 digit Bank Verification Number!</div>');
      $('#bvn').focus();
      return;
    }
    var dataString = $("#newPersonForm").serialize();
    $('#saveBTN').addClass('disabled');
    registerPerson(dataString);
  });

  // on AddIdType button click
  $('#addIDType').on('click', function (event) {
    event.preventDefault();
    if (counter >= 3) {
      $('#addIDType').addClass('disabled');
      return;
    }
    counter++;
    addIdType(counter);
  });

});

/**
 * Add Identification type
 */
var dynamicDiv = document.querySelector('#dynamic');

function addIdType(counter) {
  var idTypeDiv = document.createElement('div');
  idTypeDiv.setAttribute('id', 'idTypeDiv' + counter);
  idTypeDiv.setAttribute('class', 'dynamicWrapper col-md-12');

  var FormGroupDiv = document.createElement('div');
  FormGroupDiv.setAttribute('class', 'form-group');
  FormGroupDiv.setAttribute('id', 'div' + counter);
  var label = document.createElement('label');
  label.setAttribute('for', 'id-number' + counter);
  label.setAttribute('class', 'control-label col-md-4');
  label.innerHTML = 'ID Type';
  var SelectDiv = document.createElement('div');
  SelectDiv.setAttribute('class', 'col-md-8');
  var select = document.createElement('select');
  select.setAttribute('name', 'id-type' + counter);
  select.setAttribute('id', 'id-type' + counter);
  select.setAttribute('class', 'form-control');
  var selectOption = document.createElement('option');
  selectOption.innerText = "-- Select --";
  select.appendChild(selectOption);

  var removeBtn = document.createElement('span');
  removeBtn.setAttribute('class', 'glyphicon glyphicon-remove-circle');
  removeBtn.setAttribute('id', 'removeIDType' + counter);
  removeBtn.setAttribute('type', 'button');
  removeBtn.setAttribute('onclick', 'removeIdTypeField(event)');
  SelectDiv.appendChild(select);
  FormGroupDiv.appendChild(label);
  FormGroupDiv.appendChild(SelectDiv);
  fetchIdentificationTypes('id-type' + counter);

  var FormGroupDiv2 = document.createElement('div');
  FormGroupDiv2.setAttribute('class', 'form-group');
  FormGroupDiv2.setAttribute('id', 'div' + counter);
  var label2 = document.createElement('label');
  label2.setAttribute('for', 'id-number' + counter);
  label2.setAttribute('class', 'control-label col-md-4');
  label2.innerHTML = 'ID Number';
  var InputDiv = document.createElement('div');
  InputDiv.setAttribute('class', 'col-md-8');
  var inputField = document.createElement('input');
  inputField.setAttribute('type', 'text');
  inputField.setAttribute('name', 'id-number' + counter);
  inputField.setAttribute('id', 'id-number' + counter);
  inputField.setAttribute('class', 'form-control');
  InputDiv.appendChild(inputField);
  FormGroupDiv2.appendChild(label2);
  FormGroupDiv2.appendChild(InputDiv);

  idTypeDiv.appendChild(removeBtn);
  idTypeDiv.appendChild(FormGroupDiv);
  idTypeDiv.appendChild(FormGroupDiv2);

  dynamicDiv.appendChild(idTypeDiv);
}

/**
 * Register person
 */
function registerPerson(newPersonData) {
  hidePage();
  $.ajax({
      beforeSend: function (xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + authToken);
      },
      method: "POST",
      url: apiBaseUrl + "user",
      data: newPersonData,
      dataType: "json"
    })
    .done(function (response) {
      showPage();
      $('#saveBTN').removeClass('disabled');
      if (response.status) {
        $('#newPersonForm').prepend('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error! </strong>' + response.message + '</div>');
        setTimeout(function () {
          $('.alert').remove();
        }, 2000);
      } else {
        $('html, body').animate({
          scrollTop: 0
        }, 'fast');
        $('#newPersonForm').prepend('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success! </strong>New person registered successfully</div>');
        setTimeout(function () {
          (document.referrer) ? window.location.href = document.referrer: window.location.href = '/dashboard';
        }, 2000);
      }
    })
    .fail(function (error) {
      showPage();
      $('#saveBTN').removeClass('disabled');
      $('#newPersonForm').prepend('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error!</strong>' + error.message + '</div>');
      setTimeout(function () {
        $('.alert').remove();
      }, 3000);
    });
}

function updatePersonsDetails(userDetail) {
  $('#username').val(userDetail.username || 'NA');
  $('#first-name').val(userDetail.firstName || 'NA');
  $('#last-name').val(userDetail.lastName || 'NA');
  $('#email').val(userDetail.email || 'NA');
  $('#phone-number').val(userDetail.phoneNumber || 'NA');
}

function removeIdTypeField(event) {
  var removeBtnDiv = $(event.target).attr('id');
  var buttonParent = document.querySelector("#" + removeBtnDiv).parentNode;
  dynamicDiv.removeChild(buttonParent);
  counter--;
}

function fileChange(fileInput) {
  var fileInputId = fileInput.id;
  if (fileInput.files && fileInput.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      var imageBase64 = e.target.result;
      if (fileInputId == 'photo-input') {
        document.querySelector('#photo-preview').src = imageBase64;
        document.querySelector('#photo-url').value = imageBase64;
      } else {
        document.querySelector('#id-card-preview').src = imageBase64;
        document.querySelector('#id-url').value = imageBase64;
      }
    };

    reader.readAsDataURL(fileInput.files[0]);
  }
}

function verifyUsername(username) {
  hidePage();
  $.ajax({
      method: 'GET',
      url: apiBaseUrl + 'account/' + username,
      dataType: 'json',
      headers: {
        Authorization: 'Bearer ' + authToken
      }
    })
    .done(function (response) {
      $("#first-name").val(response.firstName);
      $("#email").val(response.email);
      $("#last-name").val(response.lastName);
      $("#phone-number").val(response.phoneNumber);
      showPage();
    })
    .fail(function (error) {
      $("#first-name").val('');
      $("#email").val('');
      $("#last-name").val('');
      $("#phone-number").val('');
      showPage();
    });
}