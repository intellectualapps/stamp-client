$(function () {
  $('#searchPersonForm').on("submit", function (event) {
    event.preventDefault();
    deleteCookie("stamp-search-person-c2tId");
    verifyInputs();
  });

  $("#searchResultTableBody").on("click", ".clickable", function (event) {
    var location = $(this).data("url");
    var c2tId = $(this).attr("id");
    setCookie("stamp-search-person-c2tId", c2tId, 1);
    window.location = location;
  });
  
});

/**
 * verify search form inputs
 */
function verifyInputs() {
  var c2t_id = $("#c2t-id");
  var first_name = $("#first-name");
  var last_name = $("#last-name");

  if ((c2t_id.val() === null || c2t_id.val() === "") && (first_name.val() === null || first_name.val() === "") && (last_name.val() === null || last_name.val() === "")) {
    $('#searchPersonForm').prepend('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error!</strong> Please fill in at least one option below</div>');
    return false;
  }
  var dataString = $("#searchPersonForm").serialize();
  $('#searchBtn').addClass('disabled');
  searchPerson(dataString);
}

/**
 * search user
 */
function searchPerson(details) {
  hidePage();
  $.ajax({
      beforeSend: function (xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + authToken);
      },
      method: "GET",
      url: apiBaseUrl + "user/search",
      dataType: "json",
      data: details
    })
    .done(function (response) {
      showPage();
      $('#searchBtn').removeClass('disabled');
      if (response.status) {
        $('#searchPersonForm').prepend('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error! </strong>' + response.message + '</div>');
      } else {
        populateResultTable(response);
      }
    })
    .fail(function (error) {
      showPage();
      console.error(error);
      $('#searchBtn').removeClass('disabled');
    });
}

/**
 * populate search person result table
 */
function populateResultTable(searchResult) {
  $('#searchResultTableBody').html('');

  var tbody = document.getElementById("searchResultTableBody");
  var searchResultLength = searchResult.length;
  if (searchResultLength > 0) {
    for (var i = 0; i < searchResultLength; i++) {
      var photoUrl = searchResult[i].photoUrl;
      var c2t_id = searchResult[i].c2tId;
      var fullName = searchResult[i].firstName + " " + searchResult[i].lastName;

      if (photoUrl === "" || photoUrl === null) {
        photoUrl = "https://stamp-176816.appspot.com/assets/img/dummy.jpg";
      }
      if (c2t_id === "" || c2t_id === null) {
        c2t_id = "NA";
      }
      if (fullName === "" || fullName === null) {
        fullName = "NA";
      }

      var tr = document.createElement("tr");
      tr.className = "clickable";
      tr.id = c2t_id;
      tr.setAttribute("data-url", "view-person");

      var photoTd = document.createElement("td");
      var photoImage = document.createElement("img");
      photoImage.src = photoUrl;
      photoTd.appendChild(photoImage);

      var c2tIdTd = document.createElement("td");
      c2tIdTd.innerHTML = c2t_id;

      var fullNameTd = document.createElement("td");
      fullNameTd.innerHTML = fullName;

      tr.appendChild(photoTd);
      tr.appendChild(c2tIdTd);
      tr.appendChild(fullNameTd);

      tbody.appendChild(tr);
    }
    var tb_st = true;
  } else {
    var tr = document.createElement("tr");
    var td = document.createElement("td");
    td.innerHTML = "<h1>No persons matching your search criteria was found</h1>";
    td.className = "center";
    td.setAttribute("colspan", "3");
    tr.appendChild(td);
    tbody.appendChild(tr);
  }
}