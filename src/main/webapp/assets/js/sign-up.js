$(function () {
  $('#regForm').on("submit", function (event) {
    event.preventDefault();
    var username_div = document.getElementById('username-div');
    if (username_div.classList.contains("has-error")) {
      $("#username").focus();
      return false;
    }
    $("#regStatus").remove();
    $("#signUpBtn").addClass("disabled");
    var dataString = $("#regForm").serialize();
    signUp(dataString);
  });
});

/**
 * Sign up
 */
function signUp(details) {
  hidePage();
  $.ajax({
    method: "POST",
    url: apiBaseUrl + "user/account",
    data: details,
    dataType: 'json'
  })
    .done(function (response) {
      showPage();
      $("#signUpBtn").removeClass("disabled");
      if (response.authToken) {
        setCookie("stamp-username", response.username);
        setCookie("stamp-authToken", response.authToken);
        $("#regForm").prepend('<div class="alert alert-success" id="regStatus">Registration successful, You will be logged-in in a moment</div>');
        setTimeout(function () {
          window.location = '/dashboard';
        }, 2000);
      } else {
        showPage();
        $("#regForm").prepend('<div class="alert alert-danger" id="regStatus">' + response.message + '</div>');
        setTimeout(function(){
          $('.alert').remove();
        },2000)
      }
    })
    .fail(function (error) {
      showPage()
      $("#signUpBtn").removeClass("disabled");
      $("#regForm").prepend('<div class="alert alert-danger" id="regStatus">' + error.message + '</div>');
      setTimeout(function(){
        $('.alert').remove();
      },2000);
    });
}