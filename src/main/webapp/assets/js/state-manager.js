'use strict';

$(function () {
    if (currentUrl.indexOf('login') != -1) {
        document.querySelector('#item-login').setAttribute('class', 'active');
    }
    else if (currentUrl.indexOf('create-account') != -1) {
        document.querySelector('#item-signup').setAttribute('class', 'active');
    }
    else if (currentUrl.indexOf('dashboard') != -1) {
        document.querySelector('#item-dashboard').setAttribute('class', 'active');
    }
    else if (currentUrl.indexOf('register') != -1) {
        document.querySelector('#item-registration').setAttribute('class', 'active');
    }
    else if (currentUrl.indexOf('search') != -1) {
        document.querySelector('#item-search').setAttribute('class', 'active');
    }
    else if (currentUrl.indexOf('verif') != -1) {
        document.querySelector('#item-verification').setAttribute('class', 'active');
    }
    else if (currentUrl.indexOf('pricing') != -1) {
        document.querySelector('#item-pricing').setAttribute('class', 'active');
    }
    else if (currentUrl.indexOf('track') != -1) {
        document.querySelector('#item-tracking').setAttribute('class', 'active');
    }
    else if (currentUrl.indexOf('tradelisting') != -1) {
        document.querySelector('#item-tradelisting').setAttribute('class', 'active');
    }else if ($('#carousel')){
        document.querySelector('#item-home').setAttribute('class', 'active');
    }

});