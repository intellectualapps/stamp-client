/**
 * @author ahassan
 * @desc General Javascript file
 */
var currentUrl = window.location.href;
var apiBaseUrl = 'https://stamp-client-bff.appspot.com/api/v1/';
var authToken = getCookie('stamp-authToken');
var identifier = getCookie("stamp-identifier");
var fullname = getCookie("stamp-fullname");
var email = getCookie("stamp-email");
var account = getCookie("stamp-account");

/**
 * Perform tasks when user is logged in
 */
$(function () {
  $("#menu_username").text(fullname);
  getUserDetail(identifier);

  var landingPage = document.querySelector('#carousel');
  var navbar = document.querySelector('#navbar');
  if ((currentUrl.indexOf('login') != -1 || currentUrl.indexOf('create-account') != -1 || landingPage) && identifier) {
    navbar.removeChild(navbar.childNodes[1]);
    $('#navbar').append('<ul class="nav navbar-nav" id="user-navbar"><li id="item-dashboard"><a href="/dashboard">Dashboard</a></li><li id="item-home"><a href="/">Home</a></li><li id="item-tradelisting"><a href="/tradelisting">TradeListing</a></li><li id="item-registration"><a href="/registration">Registration</a></li><li id="item-search"><a href="/search">Search</a></li><li id="item-verification"><a href="/verification">Verification</a></li><li id="item-tracking"><a href="/tracking">Tracking</a></li><li id="item-pricing"><a href="/pricing">Pricing</a> </li><li id="item-blog"><a href="#">Blog</a></li></ul>');
    window.location = '/dashboard';
  } else if ((currentUrl.indexOf('login') != -1 || currentUrl.indexOf('create-account') != -1 || landingPage) && !identifier) {
    navbar.removeChild(navbar.childNodes[1]);
    navbar.removeChild(navbar.childNodes[2]);
    $('#navbar').append('<ul class="nav navbar-nav navbar-right" id="visitor-navbar"><li id="item-tradelisting"><a href="/tradelisting">TradeListing</a></li><li id="item-pricing"><a href="/pricing">Pricing</a> </li><li id="item-blog"><a href="#">Blog</a></li><li id="item-signup"><a href="/create-account">Sign Up</a></li><li id="item-login"><a href="/login">Log In</a></li></ul>');
  }

  if (getCookie("stamp-account") === "company") {
    if (currentUrl.includes('/dashboard') || currentUrl.includes('/registration')) {
      $("#register-self").remove();
    }
  }
});

/**
 * Initialize page function
 */
function initializePage(id) {
  $("#menu_username").text(getCookie("stamp-fullname"));
  $("li[id=acm_switch]").remove();

  getUserCompany(id);
  getUserRelationshipCount(id);

  if (document.cookie.indexOf("stamp-logged-in-user") != -1) {
    $("ul#profile-menu").prepend('<li id="acm_switch"><a href="#" onclick="switchBackAccount(\'' + getCookie("stamp-logged-in-user") + '\'); return false;">Switch back to my account</a></li><li id="acm_switch" role="separator" class="divider"></li>');
  }

  $("#dashItemsContainer").attr("hidden", false);
  
  showPage();
}

/**
 * Date picker function
 */
function datePicker() {
  $('.form_date').datepicker({});
}

/**
 * Log out
 */
function logout() {
  deleteCookie("stamp-identifier");
  deleteCookie("stamp-account");
  deleteCookie("stamp-authToken");
  deleteCookie("stamp-email");
  deleteCookie("stamp-phoneNumber");
  deleteCookie("stamp-fullname");
  deleteCookie("stamp-logged-in-user");
  window.location = '/login';
}

/**
 * Set Cookie
 */
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

/**
 * Get Cookie
 */
function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

/**
 * Delete Cookie
 */
function deleteCookie(name) {
  setCookie(name, "", -1);
}

/**
 * Go back
 */
function goBack() {
  window.history.back();
}

/**
 * Hide and show page functions
 */
function hidePage() {
  $('#header').hide();
  $('main').hide();
  $('footer').hide();
  $('body').prepend('<div id="loader"></div>');
}

function showPage() {
  $('#loader').remove();
  $('#header').show();
  $('main').show();
  $('footer').show();
}

/**
 * Verify username availability
 */
function verifyUsername(username) {
  $("#messageBlock").remove();
  $("#username-div").removeClass("has-success has-error");
  $.ajax({
      method: "GET",
      url: apiBaseUrl + "account/verify/" + username,
      dataType: "json"
    })
    .done(function (response) {
      if (response.state) {
        $("#username-div, #contact-person-div").addClass("has-success");
        $("#username, #contact-person").attr("aria-describedby", "messageBlock");
        $("#username-div, #contact-person-div").append('<span id="messageBlock" class="help-block">Username is available.</span>');
        $("#verificationStatus").html('<i class="glyphicon glyphicon-check"></i>');
      } else {
        $("#username-div, #contact-person-div").addClass("has-error");
        $("#username, #contact-person").attr("aria-describedby", "messageBlock");
        $("#username-div, #contact-person-div").append('<span id="messageBlock" class="help-block">Username already taken.</span>');
        $("#verificationStatus").html('<i class="glyphicon glyphicon-close"></i>');
      }
    })
    .fail(function (error) {
      console.log(error.message);
    });
}

/**
 * Clear sign up message
 */
function clearSignUpMessage() {
  $("#messageBlock").remove();
  var username_div = document.getElementById('username-div');
  if (username_div.classList.contains("has-success")) {
    $("#username-div").removeClass("has-success");
  }
  if (username_div.classList.contains("has-error")) {
    $("#username-div").removeClass("has-error");
  }
}

/**
 * Fetch states
 */
function fetchStates() {
  $.ajax({
      beforeSend: function (xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + authToken);
      },
      url: apiBaseUrl + 'lookup/state',
      method: 'GET',
      dataType: "json"
    })
    .done(function (response) {
      if (response.status) {
        //returned nothing
      } else {
        var select = $('#state-of-origin');
        var select1 = $('#state-of-residence');
        var select2 = $('#state');
        $.each(response, function (key, value) {
          select.append('<option value=' + value.stateId + '>' + value.name + '</option>');
          select1.append('<option value=' + value.stateId + '>' + value.name + '</option>');
          select2.append('<option value=' + value.stateId + '>' + value.name + '</option>');
        });
      }
    })
    .fail(function (error) {
      $('#newPersonForm').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>' + error.message + '</div>');
      setTimeout(function () {
        $(".alert").remove();
      }, 3000);
    });
}

/**
 * Fetch gender options
 */
function fetchGender() {
  $.ajax({
      beforeSend: function (xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + authToken);
      },
      url: apiBaseUrl + 'lookup/gender',
      method: 'GET',
      dataType: "json"
    })
    .done(function (response) {
      if (!response.status) {
        var select = $('#gender');
        $.each(response, function (key, value) {
          select.append('<option value=' + value.genderId + '>' + value.description + '</option>');
        });
      }
    })
    .fail(function (error) {
      $('#newPersonForm').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>' + error.message + '</div>');
      setTimeout(function () {
        $(".alert").remove();
      }, 3000);
    });
}

/**
 * Fetch marital status options
 */
function fetchMaritalStatus() {
  $.ajax({
      beforeSend: function (xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + authToken);
      },
      url: apiBaseUrl + 'lookup/marital-status',
      method: 'GET',
      dataType: "json"
    })
    .done(function (response) {
      if (!response.status) {
        var select = $('#marital-status');
        $.each(response, function (key, value) {
          select.append('<option value=' + value.maritalStatusId + '>' + value.description + '</option>');
        });
      }
    })
    .fail(function (error) {
      $('#newPersonForm').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>' + error.message + '</div>');
      setTimeout(function () {
        $(".alert").remove();
      }, 3000);
    });
}

/**
 * Fetch identification types options
 */
function fetchIdentificationTypes(id_element) {
  $.ajax({
      beforeSend: function (xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + authToken);
      },
      url: apiBaseUrl + 'lookup/identity-type',
      method: 'GET',
      dataType: "json"
    })
    .done(function (response) {
      if (!response.status) {
        var select = $('#' + id_element);
        $.each(response, function (key, value) {
          select.append('<option value=' + value.identityTypeId + '>' + value.description + '</option>');
        });
      }
    })
    .fail(function (error) {
      $('#newPersonForm').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>' + error.message + '</div>');
      setTimeout(function () {
        $(".alert").remove();
      }, 3000);
    });
}

/**
 * Fetch title options
 */
function fetchTitles() {
  $.ajax({
      beforeSend: function (xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + authToken);
      },
      url: apiBaseUrl + 'lookup/title',
      method: 'GET',
      dataType: "json"
    })
    .done(function (response) {
      if (!response.status) {
        var select = $('#title');
        $.each(response, function (key, value) {
          select.append('<option value=' + value.titleId + '>' + value.description + '</option>');
        });
      }
    })
    .fail(function (error) {
      $('#newPersonForm').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>' + error.message + '</div>');
      setTimeout(function () {
        $(".alert").remove();
      }, 3000);
    });
}

/**
 * Check if user is registered
 */
function getUserDetail(id) {
  $.ajax({
      beforeSend: function (xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + authToken);
      },
      url: apiBaseUrl + "user/" + id,
      method: "GET",
      dataType: "json"
    })
    .done(function (response) {
      if (response.status) {
        $("#dashItemsContainer").prepend('<div class="alert alert-warning">You can only add relationships when you have registered yourself. <a href="/register-self">Click here to register!</a></div>');
        $("h5#newSelfRegLink").append('You are not registered, Please click <a href="/register-self"> here</a> to register yourself');
        return;
      }

      if (response.c2tId) {
        identifier = setCookie("stamp-identifier", response.c2tId);
        fullname = setCookie("stamp-fullname", response.firstName + " " + response.lastName);
        email = setCookie("stamp-email", response.email);
        setCookie("stamp-phoneNumber", response.phoneNumber);
        account = setCookie("stamp-account", "person");
        initializePage(response.c2tId);
      }

      if (currentUrl.includes('add-worker-relationship')) {
        updateEmployerFields(response);
      }

      if ((currentUrl.includes('/dashboard') || currentUrl.includes('/registration')) && response.c2tId) {
        $("#register-self").remove();
      }
    })
    .fail(function (error) {
      $('main').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>' + error.message + '</div>');
      setTimeout(function () {
        $(".alert").remove();
      }, 3000);
    });
}

/**
 * Get user details by C2tId
 */
function getUserDetailsById(id) {
  $.ajax({
      beforeSend: function (xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + authToken);
      },
      url: apiBaseUrl + "user/" + id,
      method: "GET",
      dataType: "json"
    })
    .done(function (response) {
      if (response.status) {
        $("#dashItemsContainer").prepend('<div class="alert alert-warning">You can only add relationships when you have registered yourself. <a href="/register-person">Click here to register!</a></div>');
      } else {
        if (currentUrl.includes('add-worker-relationship')) {
          updateEmployeeFields(response);
        }
      }
    })
    .fail(function (error) {
      $('main').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>' + error.message + '</div>');
      setTimeout(function () {
        $(".alert").remove();
      }, 3000);
    });
}

/**
 * Get number of relationships user created
 */
function getUserRelationshipCount(c2tId) {
  $.ajax({
      beforeSend: function (xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + authToken);
      },
      url: apiBaseUrl + "user/relationship/count",
      method: "GET",
      data: "c2t-id=" + c2tId,
      dataType: "json"
    })
    .done(function (response) {
      if (!response.status) {
        $("#number_of_relationships").text(response.count);
      }
    })
    .fail(function (error) {
      $('main').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>' + error.message + '</div>');
      setTimeout(function () {
        $(".alert").remove();
      }, 3000);
    });
}

/**
 * Get account details by username
 */
function getAccountDetail(username) {
  $.ajax({
      beforeSend: function (xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + authToken);
      },
      method: "GET",
      url: apiBaseUrl + "account/" + username,
      dataType: "json"
    })
    .done(function (response) {
      $('#saveBTN').removeClass('disabled');
      if (response.status) {
        setTimeout(function () {
          $('#newPersonForm').prepend('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error! </strong>' + response.message + '</div>');
        }, 2000);
      } else {
        if (currentUrl.includes('/register-self')) {
          document.querySelector('#username').setAttribute('readonly', '');
          document.querySelector('#email').setAttribute('readonly', '');
          document.querySelector('#heading').textContent = 'Register Self';
          updatePersonsDetails(response);
        }
      }
    })
    .fail(function (error) {
      $('#newPersonForm').prepend('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error! </strong>' + error.message + '</div>');
    });
}

/*
 * Get associated accounts
 */
function getUserCompany(id) {
  $("li[id=ac_switch]").remove();
  $.ajax({
      beforeSend: function (xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + authToken);
      },
      url: apiBaseUrl + 'user/companies?c2t-id=' + id,
      method: 'GET',
      dataType: "json"
    })
    .done(function (response) {
      if (response.length > 0) {
        $("ul#profile-menu").append('<li id="ac_switch" role="separator" class="divider"></li><li id="ac_switch" class="dropdown-header">Switch Account</li>');
        $.each(response, function (i, el) {
          $("ul#profile-menu").append('<li id="ac_switch"><a href="#" onclick="switchAccount(\'' + el.companyC2tId + '\'); return false;">' + el.companyName + '</a></li>');
        });
      }
    })
    .fail(function (error) {
      $('main').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>' + error.message + '</div>');
      setTimeout(function () {
        $(".alert").remove();
      }, 3000);
    });
}

/**
 * Switch account
 */
function switchAccount(id) {
  setCookie("stamp-logged-in-user", getCookie("stamp-identifier"));
  hidePage();
  $.ajax({
      beforeSend: function (xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + authToken);
      },
      url: apiBaseUrl + 'company/' + id,
      method: 'GET',
      dataType: 'json'
    })
    .done(function (response) {
      if (response.status) {
        return false;
      } else {
        account = setCookie("stamp-account", "company");
        identifier = setCookie("stamp-identifier", id);
        if (response.email) email = setCookie("stamp-email", response.email);
        fullname = setCookie("stamp-fullname", response.companyName);
        setCookie("stamp-phoneNumber", response.companyPhoneNumbers);

        setTimeout(function () {
          initializePage(identifier);
        }, 2000);
      }
    })
    .fail(function (error) {
      $('main').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning!</strong>' + error.message + '</div>');
      setTimeout(function () {
        $(".alert").remove();
      }, 3000);
    });
}

/**
 * Switch back account
 */
function switchBackAccount(id) {
  hidePage();
  deleteCookie("stamp-logged-in-user");
  getUserDetail(id);
}