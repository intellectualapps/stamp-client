$(function () {
  var stamp_search_person_c2tId = getCookie("stamp-search-person-c2tId");
  getSearchPersonDetails(stamp_search_person_c2tId);
});

/**
 * Get search person details
 */
function getSearchPersonDetails(stamp_search_person_c2tId) {
  hidePage();
  $.ajax({
      beforeSend: function (xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + authToken);
      },
      method: "GET",
      url: apiBaseUrl + "user/search-details",
      dataType: "json",
      data: "c2t-id=" + stamp_search_person_c2tId + "&search-types=all"
    })
    .done(function (response) {
      hidePage();
      if (response.status) {
        window.location = "/search";
      } else {
        displayRecords(response);
      }
    })
    .fail(function (error) {
      hidePage();
      console.error(error);
    });
}

/**
 * Display details
 */
function displayRecords(details) {
  var addressLine = details.userDetails.addressLine;
  var bvn = details.userDetails.bvn;
  var c2tId = details.userDetails.c2tId;
  var dob = details.userDetails.dob;
  var email = details.userDetails.email;
  var firstName = details.userDetails.firstName;
  var gender = details.userDetails.gender;
  var idUrl = details.userDetails.idUrl;
  var lastName = details.userDetails.lastName;
  var maritalStatus = details.userDetails.maritalStatus;
  var middleName = details.userDetails.middleName;
  var phoneNumber = details.userDetails.phoneNumber;
  var photoUrl = details.userDetails.photoUrl;
  var postCode = details.userDetails.postCode;
  var stateOfOrigin = details.userDetails.stateOfOrigin;
  var stateOfResidence = details.userDetails.stateOfResidence;
  var stateOfResidenceName = details.userDetails.stateOfResidenceDetails.name;
  var title = details.userDetails.userDetails;
  var town = details.userDetails.town;
  var username = details.userDetails.username;

  $("#photoUrl").load(idUrl, function (response, status, xhr) {
    if (status == "error") {
      $(this).attr('src', '/assets/img/dummy.jpg');
    } else {
      $(this).attr('src', idUrl);
    }
  });
  $("#name").append(firstName + " " + middleName + " " + lastName);
  $("#c2tId").append(c2tId);
  $("#dob").append(dob);
  $("#email").append(email);
  $("#phone").append(phoneNumber);
  $("#nin").append();
  $("#passport").append();
  $("#driversLicence").append();
  $("#votersCard").append();
  $("#bvn").append(bvn);
  $("#address").append(addressLine);
  $("#city").append(town);
  $("#state").append(stateOfResidenceName);
  $("#zipcode").append(postCode);

  var employmentRecords = details.workerRelationships;
  if (employmentRecords) {
    var employmentRecordsLength = employmentRecords.length;
    if (employmentRecordsLength > 0) {
      for (var i = 0; i < employmentRecordsLength; i++) {
        var jobDescription = employmentRecords[i].jobDescription;
        var jobTitle = employmentRecords[i].jobTitle;
        var extraId = employmentRecords[i].relationshipDetails.extraId;
        var primaryC2tId = employmentRecords[i].relationshipDetails.primaryC2tId;
        var relationshipId = employmentRecords[i].relationshipDetails.relationshipId;
        var relationshipType = employmentRecords[i].relationshipDetails.relationshipType;
        var secondaryC2tId = employmentRecords[i].relationshipDetails.secondaryC2tId;
        var startDate = employmentRecords[i].relationshipDetails.startDate;
        var tertiaryC2tId = employmentRecords[i].relationshipDetails.tertiaryC2tId;
        var firstName = employmentRecords[i].relationshipDetails.primaryUser.firstName;
        var lastName = employmentRecords[i].relationshipDetails.primaryUser.lastName;

        var employmentRecordDiv = document.createElement("div");
        employmentRecordDiv.className = "col-md-4 col-sm-12";

        var employmentRecordInnerDiv = document.createElement("div");
        employmentRecordInnerDiv.className = "well";

        var addedByParagraph = document.createElement("p");
        addedByParagraph.innerText = "Added by: " + firstName + " " +lastName;

        var relationshipParagraph = document.createElement("p");
        relationshipParagraph.innerText = "Relationship: " + relationshipType;

        var startDateParagraph = document.createElement("p");
        startDateParagraph.innerText = "Start date: " + startDate;

        var endDateParagraph = document.createElement("p");
        endDateParagraph.innerText = "End date: ";

        var detailsParagraph = document.createElement("p");
        detailsParagraph.innerText = "Details: ";

        var jobTitleParagraph = document.createElement("p");
        jobTitleParagraph.innerText = "Job title: " + jobTitle;

        var jobDescriptionParagraph = document.createElement("p");
        jobDescriptionParagraph.innerText = "Job description: " + jobDescription;

        var ratingParagraph = document.createElement("p");
        ratingParagraph.innerText = "Rating: ";

        employmentRecordInnerDiv.append(addedByParagraph);
        employmentRecordInnerDiv.append(relationshipParagraph);
        employmentRecordInnerDiv.append(startDateParagraph);
        employmentRecordInnerDiv.append(endDateParagraph);
        employmentRecordInnerDiv.append(detailsParagraph);
        employmentRecordInnerDiv.append(jobTitleParagraph);
        employmentRecordInnerDiv.append(jobDescriptionParagraph);
        employmentRecordInnerDiv.append(ratingParagraph);

        employmentRecordDiv.append(employmentRecordInnerDiv);

        $("#employmentRecords").append(employmentRecordDiv);
      }
    } else {
      $("#employmentRecords").append("<h4>No employment records found</h4>");
    }
  } else {
    $("#employmentRecords").append("<h4>No employment records found</h4>");
  }

  var businessRecords = details.businessPartnerships;
  if (businessRecords) {
    var businessRecordsLength = businessRecords.length;
    if (businessRecordsLength > 0) {
      // TODO: business partnership records
    }
  } else {
    $("#businessRecords").append("<h4>No business partnerships found</h4>");
  }

  var landlordRelationships = details.landlordRelationships;
  if (landlordRelationships) {
    var landlordRelationshipsLength = landlordRelationships.length;
    if (landlordRelationshipsLength > 0) {
      // TODO: landlord relationships records
    }
  } else {
    $("#landlordRecords").append("<h4>No landlord records found</h4>");
  }

  var tenancyRecords = details.tenancyRecords;
  if (tenancyRecords) {
    var tenancyRecordsLength = tenancyRecords.length;
    if (tenancyRecordsLength > 0) {
      // TODO: tenancy relationships records
    }
  } else {
    $("#tenancyRecords").append("<h4>No tenancy records found</h4>");
  }

}